//
// Created by wolverine on 18/06/17.
//

#include "LCD_emulator.h"
#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>

static SDL_Window *LCD_display;
static SDL_Renderer *LCD_renderer;
static SDL_Surface *LCD_surface;
static SDL_Texture *LCD_texture;

static LCD_BACKLIGHT __backlight;
static uint8_t __inverted;
static sFONT *__font;
static uint16_t __foreground, __background;
static struct {
    uint16_t x,y,w,h;
} __address_window, __current_position;

SDL_Rect wholeScreen = {0, 0, 320, 240};


void LCD_init()
{
    SDL_Init(SDL_INIT_EVERYTHING);

    LCD_display = SDL_CreateWindow("LCD display", 100, 100, 320, 240,  SDL_WINDOW_SHOWN);
    if (LCD_display==NULL)
    {
        fprintf(stderr, "Error with SDL_CreateWindow\n");
        SDL_Quit();
        exit(2);
    }

    LCD_renderer = SDL_CreateRenderer(LCD_display, -1, SDL_RENDERER_ACCELERATED);
    if (LCD_renderer == NULL)
    {
        fprintf(stderr, "Error creating the renderer\n");
        SDL_DestroyWindow(LCD_display);
        SDL_Quit();
        exit(2);
    }

    Uint16 rmask, gmask, bmask, amask;

    rmask = 0xf800;
    gmask = 0x07E0;
    bmask = 0x001F;
    amask = 0x0000;


    LCD_surface = SDL_CreateRGBSurface(0, 320, 240, 16, rmask, gmask, bmask, amask);
    if (LCD_surface == NULL)
    {
        SDL_DestroyWindow(LCD_display);
        SDL_DestroyRenderer(LCD_renderer);
        SDL_Quit();
        exit(2);
    }

    __backlight = BACKLIGHT_OFF;
    __inverted = 0;
}

void LCD_set_backlight(LCD_BACKLIGHT mode)
{
    __backlight = mode;
}

void LCD_set_rotation(uint8_t m)
{

}

void LCD_invert_display(uint8_t i)
{
    __inverted = i;
}

void LCD_set_address_window(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
    __address_window.x = x;
    __address_window.y = y;
    __address_window.h = h;
    __address_window.w = w;

    __current_position = __address_window;
}

void LCD_write_pixel(uint16_t color)
{
    ((uint16_t*)LCD_surface->pixels)[__current_position.x + __current_position.y * 320] = color;
    if (++__current_position.x == (__address_window.x +__address_window.w))
    {
        __current_position.x = __address_window.x;
        if (++__current_position.y == (__address_window.y + __address_window.h))
            __current_position.y = __address_window.y;
    }

}

void LCD_write_pixels(uint16_t *color, uint32_t length)
{
    uint32_t cnt;
    for(cnt=0;cnt<length;cnt++)
        LCD_write_pixel((color[cnt]>>8) | ((color[cnt]&0xFF)<<8));

    SDL_Texture *texture = SDL_CreateTextureFromSurface(LCD_renderer, LCD_surface);
    SDL_RenderCopy(LCD_renderer, texture, &wholeScreen, &wholeScreen);
    SDL_DestroyTexture(texture);
    SDL_RenderPresent(LCD_renderer);
}

void LCD_set_font(sFONT *font)
{
    __font = font;
}

void LCD_set_foreground(uint16_t color)
{
    __foreground = color;
}

void LCD_set_background(uint16_t color)
{
    __background = color;
}

void LCD_print(uint16_t x, uint16_t y, char* text)
{
    uint16_t string_length = strlen(text);

    if (__font == NULL)
        return;

    uint16_t width = __font->Width * string_length;
    uint16_t height = __font->Height;
    uint16_t i, j, k;

    LCD_set_address_window(x, y, width, height);

    for (j=0;j<height;j++)
        for(i=0;i<string_length;i++)
            for (k=0;k<__font->Width;k++)
            {
                int num = k/8 + (__font->Width/8 + 1) * j;
                int bit = ((0x80)>>k%8);
                int origin = (text[i]-0x20) * ((__font->Width/8 + 1) * __font->Height);

                if (__font->table[origin + num] & bit)
                    LCD_write_pixel(__foreground);
                else
                    LCD_write_pixel(__background);
            }

    SDL_Texture *texture = SDL_CreateTextureFromSurface(LCD_renderer, LCD_surface);
    SDL_RenderCopy(LCD_renderer, texture, &wholeScreen, &wholeScreen);
    SDL_DestroyTexture(texture);
    SDL_RenderPresent(LCD_renderer);
}

void LCD_fill_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color)
{
    uint32_t cnt;
    LCD_set_address_window(x,y,w,h);
    for (cnt=0;cnt<(uint32_t)h*w;cnt++)
    {
        LCD_write_pixel(color);
    }

    SDL_Texture *texture = SDL_CreateTextureFromSurface(LCD_renderer, LCD_surface);
    SDL_RenderCopy(LCD_renderer, texture, &wholeScreen, &wholeScreen);
    SDL_DestroyTexture(texture);
    SDL_RenderPresent(LCD_renderer);
}
