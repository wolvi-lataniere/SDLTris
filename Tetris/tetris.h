//
// Created by wolverine on 18/06/17.
//

#ifndef SDLTRIS_TETRIS_H
#define SDLTRIS_TETRIS_H

#include "../keys.h"

#ifdef __cplusplus
extern "C"{
#endif


void tetrisInit();
int tetrisUpdate(KEYS active_keys, KEYS click_keys);



#ifdef __cplusplus
};
#endif

#endif //SDLTRIS_TETRIS_H
