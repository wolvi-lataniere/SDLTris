//
// Created by wolverine on 18/06/17.
//

#include <stdint.h>
#include <stdlib.h>
#include "tetris.h"
#include "../LCD_emulator.h"
#include <SDL.h>
#include <time.h>
#include "../keys.h"

extern uint16_t backgroundTetris[];
extern const uint16_t blockTile[];

uint8_t fullLines[4];
uint8_t game_speed = 25;
uint8_t speed_counter = 0;
uint16_t piece_count = 0;
uint16_t score;
uint16_t lines = 0;
uint8_t next_tetromino=0;


void tetrisDrawGrid();

#define GRID_WIDTH 10
#define GRID_HEIGHT 22
#define GRID_OFFSCREEN 2
#define BLOCK_WIDTH 11
#define BLOCK_HEIGHT 11
#define SCREEN_OFFSET_X 110
#define SCREEN_OFFSET_Y 8
#define GRID_BLANK_VALUE 8
#define GRID_ERASE_VALUE 7

// Tetrominos
typedef uint16_t TETROMINO;

const TETROMINO tetrominos[] = {
        0b0010001000100010,  // I
        0b0000011001100000,  // O
        0b0110010001000000,  // L
        0b0110001000100000,  // J
        0b0010011001000000,  // Z
        0b0100011000100000,  // S
        0b0010011000100000,  // T
};

const uint16_t tetrominoColors[] =
{
    ILI9341_CYAN,
    ILI9341_YELLOW,
    ILI9341_NAVY,
    ILI9341_ORANGE,
    ILI9341_RED,
    ILI9341_GREEN,
    ILI9341_PINK,
    ILI9341_BLACK,
    ILI9341_DARKGREY
};

uint8_t game_grid[GRID_HEIGHT*GRID_WIDTH];

typedef enum{
    TETRIS_PLAYING,
    TETRIS_GAMEOVER
}TETRIS_MODE;

static TETRIS_MODE _game_mode;

// Functions


/**
 * Game initialization
 */
void tetrisInit()
{
    uint16_t i=0;

    LCD_set_address_window(0,0,320, 240);
    LCD_write_pixels(backgroundTetris, 320*240);

    for (i=SCREEN_OFFSET_Y;i<240;i+=12)
    {
        LCD_set_address_window(98, i, 12, 12);
        LCD_write_pixels(blockTile, 12*12);
    }
    for (i=SCREEN_OFFSET_Y;i<240;i+=12)
    {
        LCD_set_address_window(220, i, 12, 12);
        LCD_write_pixels(blockTile, 12*12);
    }
    for (i=110;i<208;i+=12)
    {
        LCD_set_address_window(i, 228, 12, 12);
        LCD_write_pixels(blockTile, 12*12);
    }

    for (i=0;i<GRID_HEIGHT*GRID_WIDTH;i++)
        game_grid[i]=GRID_BLANK_VALUE;


    _game_mode = TETRIS_PLAYING;

    score=0;
    piece_count = 0;
    game_speed = 25;
    lines = 0;
    next_tetromino = rand() % 7;
    tetrisDrawGrid();

    srand(time(0));
}


uint8_t tetrominoGetXY(uint8_t id, uint8_t rotation, uint8_t x, uint8_t y)
{
    uint8_t index;
    if (id>=7)
        return 0;

    switch (rotation%4)
    {
        case 0:
            index = x+(y*4);
            break;
        case 1:
            index = 12 + y - x*4;
            break;
        case 2:
            index = 15 - (x + y*4);
            break;
        case 3:
            index = 3 - y + (x*4);
            break;
    }

    return (tetrominos[id] & (0x8000>>index)) != 0;
}

void tetrisDrawTetromino(uint8_t id, uint8_t rotation, int8_t x, int8_t y, uint8_t erase)
{
    int i,j;

    for (i=0;i<4;i++)
        for (j=0;j<4; j++)
        {
            if (tetrominoGetXY(id, rotation, i, j) != 0 && (j+y)>=GRID_OFFSCREEN)
            {
                LCD_fill_rect(SCREEN_OFFSET_X + (x+i)*BLOCK_WIDTH,
                              SCREEN_OFFSET_Y+ (y+j-GRID_OFFSCREEN)*BLOCK_HEIGHT,
                              BLOCK_WIDTH, BLOCK_HEIGHT,
                              erase?ILI9341_DARKGREY:tetrominoColors[id] );
            }
        }
}


uint8_t tetrominoCheckCollision(uint8_t id, uint8_t rotation, int8_t x, int8_t y)
{
    int8_t i,j;
    if (id>=7)
        return 1;

    for (i=0;i<4;i++)
        for (j=0;j<4;j++)
        {
            if (tetrominoGetXY(id, rotation, i, j))
            {
                if (x+i<0 || (x+i)>=GRID_WIDTH)
                    return 1;
                if ((j+y) >= GRID_HEIGHT)
                    return 1;
                if (game_grid[(i+x)+GRID_WIDTH*(j+y)] != GRID_BLANK_VALUE)
                    return 1;
            }
        }

    return 0;
}

void pushTetromino(uint8_t id, uint8_t rotation, int8_t x, int8_t y)
{
    int8_t i,j;
    if (id>=7)
        return;

    for (i=0;i<4;i++)
        for (j=0;j<4;j++)
        {
            if (tetrominoGetXY(id, rotation, i, j))
                game_grid[(x+i)+((y+j)*GRID_WIDTH)] = id;
        }
}


void tetrisDrawGrid()
{
    int8_t x, y;
    char sScore[30];

    for (x=0;x<GRID_WIDTH;x++)
        for (y=GRID_OFFSCREEN;y<GRID_HEIGHT;y++)
            LCD_fill_rect(SCREEN_OFFSET_X + x*BLOCK_WIDTH,
                          SCREEN_OFFSET_Y + (y-GRID_OFFSCREEN)*BLOCK_HEIGHT,
                           BLOCK_WIDTH, BLOCK_HEIGHT,
                           tetrominoColors[game_grid[x+(y*GRID_WIDTH)]]);

    LCD_set_foreground(ILI9341_BLACK);
    LCD_set_background(3167);
    LCD_set_font(&Font12);
    sprintf(sScore, "%d", score);
    LCD_fill_rect(240, 150, 80, 80, 3167);
    LCD_print(240, 150, "Score :");
    LCD_print(240, 162, sScore);
    LCD_print(240, 180, "Line :");
    sprintf(sScore, "%d", lines);
    LCD_print(240, 192, sScore);

    LCD_fill_rect(25, 30, 60, 60, ILI9341_DARKGREY);
    for (x=0;x<4;x++)
        for (y=0;y<4;y++)
            if (tetrominoGetXY(next_tetromino, 0, x, y))
                LCD_fill_rect(30 + BLOCK_WIDTH*x, 35 + BLOCK_HEIGHT*y,
                BLOCK_WIDTH, BLOCK_HEIGHT, tetrominoColors[next_tetromino]);


}

void tetrisCheckFullLine(uint8_t last_y)
{
    uint8_t x,y;

    for (y=0;y<4;y++)
    {
        fullLines[y] = 1;

        if (y+last_y>=GRID_HEIGHT) {
            fullLines[y]=0;
            break;
        }
        for (x=0;x<GRID_WIDTH;x++)
        {
            if (game_grid[x + ((y+last_y)*GRID_WIDTH)]==GRID_BLANK_VALUE)
                fullLines[y] = 0;
        }
    }
}

/**
 * Game update
 * @param active_keys
 */
int tetrisUpdate(KEYS active_keys, KEYS click_keys)
{
    switch (_game_mode)
    {
        case TETRIS_PLAYING:
        {
            static int8_t x = 4, y=1;
            static uint8_t rotation = 0, id=9;
            uint8_t forced_move=0, pushback = 0;

            if (++speed_counter == game_speed)
            {
                forced_move=1;
                speed_counter=0;
            }

            if (id>=7)
                id=rand()%7;

            if (click_keys.up)
            {
                // TODO: Handle Wall Kick
                tetrisDrawTetromino(id, rotation, x, y, 1);
                if (!tetrominoCheckCollision(id, rotation+1, x, y))
                    rotation++;
            }

            if (click_keys.back)
            {
                tetrisDrawTetromino(id, rotation, x, y, 1);
                if (!tetrominoCheckCollision(id, rotation, x-1, y))
                    x--;
            }
            if (click_keys.enter)
            {
                tetrisDrawTetromino(id, rotation, x, y, 1);
                if (!tetrominoCheckCollision(id, rotation, x+1, y))
                    x++;
            }
            if (active_keys.down || forced_move)
            {
                tetrisDrawTetromino(id, rotation, x, y, 1);
                if (!tetrominoCheckCollision(id, rotation, x, y+1))
                    y++;
                else pushback=1;
            }


            if (pushback)
            {
                uint8_t erased = 0;

                pushTetromino(id, rotation, x, y);

                score +=10;

                tetrisCheckFullLine(y);

                uint8_t i;
                for (i=0;i<4;i++)
                {
                    if (fullLines[i] == 1  && i+y<GRID_HEIGHT)
                    {
                        uint8_t j;
                        for (j=0;j<GRID_WIDTH;j++)
                        {
                            game_grid[j+((i+y)*GRID_WIDTH)] = GRID_ERASE_VALUE;
                        }
                        erased++;
                    }
                }

                score += 100*erased*erased;

                if(erased)
                {
                    tetrisDrawGrid();

                    for (i=0;i<4;i++)
                    {
                        if (fullLines[i])
                        {
                            lines++;
                            uint8_t j,k;
                            for (k=i+y;k>0;k--)
                                for (j=0;j<GRID_WIDTH;j++)
                                {
                                    game_grid[j+((k)*GRID_WIDTH)] = game_grid[j+((k-1)*GRID_WIDTH)];
                                }
                            for (j=0;j<GRID_WIDTH;j++)
                            {
                                game_grid[j] = GRID_BLANK_VALUE;
                            }
                        }
                    }

                    SDL_Delay(500);
                }
                x=4;y=1;
                rotation = 0;
                id=next_tetromino;
                next_tetromino=rand()%7;

                if ((++piece_count)%10 == 0){
                    game_speed--;
                }

                tetrisDrawGrid();

                // Handling Game Over
                if (tetrominoCheckCollision(id, rotation, x, y))
                {
                    LCD_set_address_window(0, 0, 320, 240);
                    LCD_write_pixels(backgroundTetris, 320*240);
                    LCD_set_font(&Font24);
                    LCD_fill_rect(50, 40, 220, 100, 3167);
                    LCD_print(85, 45, "GAME OVER");
                    LCD_print(100, 70, "Score");
                    char sScore[25];
                    snprintf(sScore, 25, "%d", score);
                    LCD_print(105, 100, sScore);
                    _game_mode = TETRIS_GAMEOVER;
                }


            }

            tetrisDrawTetromino(id, rotation, x, y, 0);

            break;
        }

        case TETRIS_GAMEOVER:
            if (click_keys.enter)
            {
                tetrisInit();
            }
            if (click_keys.back)
                return -1;
            break;
    }

    return 0;

}