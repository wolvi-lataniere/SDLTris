#include <iostream>
#include <SDL.h>
#include "keys.h"
#include "Tetris/tetris.h"
#include "LCD_emulator.h"

extern "C"{
extern const uint16_t image[];
}

bool running=true;

KEYS pre_keys, active_keys;
KEYS clicked_keys;

typedef enum {
    MODE_INIT,
    MODE_TETRIS
} APP_MODE;

APP_MODE __mode;

void handle_events()
{
    SDL_Event event;

    if (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
                running = false;
                break;

            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_LEFT:
                        active_keys.back = 1;
                        break;
                    case SDLK_RIGHT:
                        active_keys.enter = 1;
                        break;
                    case SDLK_UP:
                        active_keys.up = 1;
                        break;
                    case SDLK_DOWN:
                        active_keys.down=1;
                        break;
                    case SDLK_ESCAPE:
                        running=false;
                        break;
                    case SDLK_SPACE:
                        active_keys.space =1;
                        break;
                }
                break;

            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                    case SDLK_LEFT:
                        active_keys.back = 0;
                        break;
                    case SDLK_RIGHT:
                        active_keys.enter = 0;
                        break;
                    case SDLK_UP:
                        active_keys.up = 0;
                        break;
                    case SDLK_DOWN:
                        active_keys.down=0;
                        break;
                    case SDLK_SPACE:
                        active_keys.space =0;
                        break;
                }
                break;

            default:
                break;
        }
    }

    if (pre_keys.back==0 && active_keys.back==1)
        clicked_keys.back=1;
    else
        clicked_keys.back=0;


    if (pre_keys.up==0 && active_keys.up==1)
        clicked_keys.up=1;
    else
        clicked_keys.up=0;


    if (pre_keys.down==0 && active_keys.down==1)
        clicked_keys.down=1;
    else
        clicked_keys.down=0;


    if (pre_keys.enter==0 && active_keys.enter==1)
        clicked_keys.enter=1;
    else
        clicked_keys.enter=0;

    if (pre_keys.space==0 && active_keys.space==1)
        clicked_keys.space=1;
    else
        clicked_keys.space=0;

    pre_keys = active_keys;
}


int main(int argc, char** argv) {
    std::cout << "Hello, World!" << std::endl;

    __mode = MODE_INIT;

    LCD_init();

    LCD_set_foreground(ILI9341_BLACK);
    LCD_set_background(ILI9341_WHITE);
    LCD_set_font(&Font24);

    LCD_set_address_window(0, 0, 320, 240);
    LCD_write_pixels((uint16_t *)image, 320*240);

    while(running)
    {
        handle_events();

        switch (__mode)
        {
            case MODE_INIT:
                if (clicked_keys.enter)
                {
                    __mode = MODE_TETRIS;
                    tetrisInit();
                    std::cout << "Enter" << std::endl;
                }
                break;

            case MODE_TETRIS:
                if (tetrisUpdate(active_keys, clicked_keys) == -1)
                {
                    __mode = MODE_INIT;
                    LCD_set_address_window(0,0,320, 240);
                    LCD_write_pixels((uint16_t*)image, 320*240);
                }

                break;

        }


        SDL_Delay(20);
    }

    SDL_Quit();
    return 0;
}