//
// Created by wolverine on 18/06/17.
//

#ifndef SDLTRIS_LCD_EMULATOR_H
#define SDLTRIS_LCD_EMULATOR_H

#include <stdint.h>
#include "Utilities/fonts.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef enum {
    BACKLIGHT_OFF,
    BACKLIGHT_ON
} LCD_BACKLIGHT;

typedef enum {
    LCD_OK,
    LCD_ERROR
} LCD_STATUS;


// Color definitions
#define ILI9341_BLACK       0x0000      /*   0,   0,   0 */
#define ILI9341_NAVY        0x000F      /*   0,   0, 128 */
#define ILI9341_DARKGREEN   0x03E0      /*   0, 128,   0 */
#define ILI9341_DARKCYAN    0x03EF      /*   0, 128, 128 */
#define ILI9341_MAROON      0x7800      /* 128,   0,   0 */
#define ILI9341_PURPLE      0x780F      /* 128,   0, 128 */
#define ILI9341_OLIVE       0x7BE0      /* 128, 128,   0 */
#define ILI9341_LIGHTGREY   0xC618      /* 192, 192, 192 */
#define ILI9341_DARKGREY    0x7BEF      /* 128, 128, 128 */
#define ILI9341_BLUE        0x001F      /*   0,   0, 255 */
#define ILI9341_GREEN       0x07E0      /*   0, 255,   0 */
#define ILI9341_CYAN        0x07FF      /*   0, 255, 255 */
#define ILI9341_RED         0xF800      /* 255,   0,   0 */
#define ILI9341_MAGENTA     0xF81F      /* 255,   0, 255 */
#define ILI9341_YELLOW      0xFFE0      /* 255, 255,   0 */
#define ILI9341_WHITE       0xFFFF      /* 255, 255, 255 */
#define ILI9341_ORANGE      0xFD20      /* 255, 165,   0 */
#define ILI9341_GREENYELLOW 0xAFE5      /* 173, 255,  47 */
#define ILI9341_PINK        0xF81F

// Public functions
void LCD_init();

void LCD_set_backlight(LCD_BACKLIGHT mode);

void LCD_set_rotation(uint8_t m);

void LCD_invert_display(uint8_t i);

void LCD_set_address_window(uint16_t x, uint16_t y, uint16_t w, uint16_t h);

void LCD_write_pixel(uint16_t color);

void LCD_write_pixels(uint16_t *color, uint32_t length);

void LCD_set_font(sFONT *font);

void LCD_set_foreground(uint16_t color);

void LCD_set_background(uint16_t color);

void LCD_print(uint16_t x, uint16_t y, char *text);

void LCD_fill_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);

#ifdef __cplusplus
};
#endif

#endif //SDLTRIS_LCD_EMULATOR_H
